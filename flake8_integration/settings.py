#-*- coding: utf-8 -*-

"""Module used to define how to manage settings and their persistence

:author: Sébastien CHAZALLET <s.chazallet@gmail.com>
:organization: InsPyration EURL
:copyright: Copyright © InsPyration EURL <www.inspyration.org>
:license: GPL 3 <http://www.gnu.org/licenses/gpl.html>

:version: 0.1
"""

from gettext import gettext as _

from gi.repository import Gio

FLAKE8_KEY_BASE = 'org.gnome.gedit.plugins.flake8_integration'

FLAKE8_KEY_W191 = "w191"
FLAKE8_KEY_W291 = "w291"
FLAKE8_KEY_W292 = "w292"
FLAKE8_KEY_W293 = "w293"
FLAKE8_KEY_W391 = "w391"

FLAKE8_IGNORED_KEYS = (FLAKE8_KEY_W191, FLAKE8_KEY_W291, FLAKE8_KEY_W292,
                       FLAKE8_KEY_W293, FLAKE8_KEY_W391)

CONFIG_OPTIONS = {FLAKE8_KEY_W191: _("w191: identation contains tabs"),
                  FLAKE8_KEY_W291: _("w291: trailling whitespace"),
                  FLAKE8_KEY_W292: _("w292: no new line at end of file"),
                  FLAKE8_KEY_W293: _("w293: blank line contains whitespace"),
                  FLAKE8_KEY_W391: _("w391: blank line at end of file")}

FLAKE8_KEY_COMPLEXITY = "complexity"


class SettingsManager(object):
    """Allow to manage local settings and it persistent storage"""

    def __init__(self):
        """Initialize Settings Manager

        :type  self: SettingsManager
        :param self: Current manager

        :rtype: None
        """

        #_settings allow persistent storage of configuration
        self._settings = Gio.Settings.new(FLAKE8_KEY_BASE)
        #settings contains configuration usable in the plugin
        self.settings = {}
        # pull local settings from persistent storage
        self.pull_settings()

    def pull_settings(self):
        """Pull operation update local settings from persistent storage

        :type  self: SettingsManager
        :param self: Current manager

        :rtype: None
        """

        # pull ignore list
        for tag in FLAKE8_IGNORED_KEYS:
            self.settings[tag] = self._settings.get_boolean(tag)
        # pull complexity
        self.settings[FLAKE8_KEY_COMPLEXITY] = self._settings.get_int(
            FLAKE8_KEY_COMPLEXITY)

    def push_settings(self):
        """Push operation allow persistent storage update from local settings

        :type  self: SettingsManager
        :param self: Current manager

        :rtype: None
        """

        # push ignore list
        for tag in FLAKE8_IGNORED_KEYS:
            self._settings.set_boolean(tag, self.settings[tag])
        # push complexity
        self._settings.set_int(FLAKE8_KEY_COMPLEXITY,
                               self.settings[FLAKE8_KEY_COMPLEXITY])

    def update_setting(self, values={}):
        """Updating settings allow to update local and persistent settings

        :type  self: SettingsManager
        :param self: Current manager
        :type  values: dict
        :param values: values to store persistently

        :rtype: None
        """
        # update local settings
        self.settings.update(values)
        # push local settings to persistent storage
        self.push_settings()

    def get_ignore_keys(self):
        """Allow to share information about keys used to store ignore list

        :type  self: SettingsManager
        :param self: Current manager

        :rtype: list of str
        :return: list of ignore keys
        """
        return FLAKE8_IGNORED_KEYS

    def get_ignore_items(self):
        """Allow to share information about keys used to store ignore list

        :type  self: SettingsManager
        :param self: Current manager

        :rtype: list of tuple of str
        :return: list of (ignore key, ignore label)
        """
        return CONFIG_OPTIONS.items()

    def get_complexity_key(self):
        """Allow to share information about key used to store complexity

        :type  self: SettingsManager
        :param self: Current manager

        :rtype: str
        :return: name of the complexity key
        """
        return FLAKE8_KEY_COMPLEXITY

    def is_in_ignore_list(self, key):
        """Answer to "Is tag in ignore list ?" question

        :type  self: SettingsManager
        :param self: Current manager
        :type  key: str
        :param key: flake8 key as defined in FLAKE8_IGNORED_KEYS

        :rtype: bool
        :return: True if the key is in ignore list, False otherwise
        """
        return self.settings[key]

    def get_ignore_list(self):
        """Get the user defined warning keys to ignore

        :type  self: SettingsManager
        :param self: Current manager

        :rtype: list of str
        :return: list of warning keys
        """
        return [tag for tag, value in self.settings.items()
                if tag in FLAKE8_IGNORED_KEYS and value is True]

    def get_complexity(self):
        """Get the user defined complexity parameter

        :type  self: SettingsManager
        :param self: Current manager

        :rtype: int
        :return: complexity
        """
        return self.settings[FLAKE8_KEY_COMPLEXITY]
