#-*- coding: utf-8 -*-

"""Module managing the tree and all stuff around it

:author: Sébastien CHAZALLET <s.chazallet@gmail.com>
:organization: InsPyration EURL
:copyright: Copyright © InsPyration EURL <www.inspyration.org>
:license: GPL 3 <http://www.gnu.org/licenses/gpl.html>

:version: 0.1
"""


from gettext import gettext as _

from gi.repository import Gtk

SP_COL_TITLES = (_("Code"), _("L"), _("C"), _("Errors / Warnings"))
SP_COL_TYPES = (str, int, int, str)

PYTHON_ICON = 'gnome-mime-text-x-python'


class GeditFlake8Tree(Gtk.TreeView):
    """Tree inside gedit side panel containing all flake8 warnings"""

    def __init__(self, window):
        """Configure tree columns, tree selection and add it to side panel

        :type  self: GeditFlake8Tree
        :param self: Current tree
        :type  window: Gedit.Window
        :param window: GEdit window object

        :rtype: None
        """

        # use a list store to manage warnings
        Gtk.TreeView.__init__(self, Gtk.ListStore(*SP_COL_TYPES))

        # compose window
        self.window = window

        # create columns
        for index, title in enumerate(SP_COL_TITLES):
            self.append_column(Gtk.TreeViewColumn(
                _(title),
                Gtk.CellRendererText(),
                text=index))

        # Allow to do something when a tree line is selected
        self.get_selection().connect("changed", self.on_tree_selection)

        # create an icon for the side panel
        image = Gtk.Image()
        image.set_from_icon_name(PYTHON_ICON, Gtk.IconSize.MENU)

        # get the side panel (created by GEdit)
        sp = self.window.get_side_panel()

        # Add the tree in the side panel
        sp.add_item(
            self,
            'flake8_results',
            _("Flake8 results"),
            image
        )

        # set the side panel invisible
        sp.set_property("visible", False)

    def remove(self):
        """remove the tree from the side panel

        :type  self: GeditFlake8Tree
        :param self: Current tree

        :rtype: None
        """

        # get the side panel (created by GEdit)
        sp = self.window.get_side_panel()
        sp.remove_item(self)

    def activate(self):
        """show the tree in the side panel
        
        :type  self: GeditFlake8Tree
        :param self: Current tree
        
        :rtype: None
        """

        sp = self.window.get_side_panel()
        sp.set_property("visible", True)
        sp.activate_item(self)

    def on_tree_selection(self, selection):
        """When a tree line is selected, go to the file linked line

        :type  self: GeditFlake8Tree
        :param self: Current tree
        :type  selection: Gtk.TreeSelection
        :param selection: Current selection

        :rtype: None
        """

        doc = self.window.get_active_document()
        if not doc:
            return

        # get the tree line selected and put the cursor to the file linked line
        model, treeiter = selection.get_selected()
        doc.goto_line(model[treeiter][1] - 1)

        # scroll the view to the cursor
        self.window.get_active_view().scroll_to_cursor()

    def update_model(self, datas):
        """Update the tree with new datas

        :type  self: GeditFlake8Tree
        :param self: Current tree
        :type  datas: list
        :param datas: list of ready to use (str, int, int, str) tuples

        :rtype: None
        """

        # get the model (Gtk.ListStore)
        model = self.get_model()

        # Empty model
        model.clear()

        # Fill model with new values
        for data in datas:
            model.append(data)
