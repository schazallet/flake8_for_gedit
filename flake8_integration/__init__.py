#-*- coding: utf-8 -*-
"""Plugin for gedit that allow flake8 integration

:author: Sébastien CHAZALLET <s.chazallet@gmail.com>
:organization: InsPyration EURL
:copyright: Copyright © InsPyration EURL <www.inspyration.org>
:license: GPL 3 <http://www.gnu.org/licenses/gpl.html>

:version: 0.1
"""

from windowactivatable import GeditFlake8WindowActivatable
