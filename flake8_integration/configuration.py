#-*- coding: utf8 -*-

"""Module used to configure plugin from an easy-to-use widget

:author: Sébastien CHAZALLET <s.chazallet@gmail.com>
:organization: InsPyration EURL
:copyright: Copyright © InsPyration EURL <www.inspyration.org>
:license: GPL 3 <http://www.gnu.org/licenses/gpl.html>

:version: 0.1
"""

from gettext import gettext as _

from gi.repository import Gtk


STUFF = 1


class GeditFlake8ConfigWidget(Gtk.Box):

    def __init__(self, settings):
        """Initialize the configuration widget from SettingsManager properties

        :type  self: GeditFlake8ConfigWidget
        :param self: Current widget

        :type  settings: SettingsManager
        :param settings: Object used to define all configurable properties

        :rtype: None
        """

        # the current widget is a box
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=6)

        # reference to settings
        self.settings = settings

        # store useful entries into a dictionary
        # in order to get their values later
        self.entries = {}

        # Add a label on the top ot widget
        self.pack_start(Gtk.Label(_("Add to ignore list")), True, True, 0)

        # create one checkbox for each warning that can be ignored
        for key, option in self.settings.get_ignore_items():
            button = Gtk.CheckButton(option)
            button.set_active(self.settings.is_in_ignore_list(key))
            self.pack_start(button, True, True, 0)
            # add the check button to entries
            self.entries[key] = button

        # create an Hbox to get the label and the entry on the same line
        hbox = Gtk.Box(spacing=3)
        self.pack_start(hbox, True, True, 0)

        # label for complexity entry
        hbox.pack_start(Gtk.Label(_("Complexity")), True, True, 0)

        # complexity entry is an widget allowing us to be sure to have an int
        complexity_entry = Gtk.SpinButton()
        complexity_entry.set_adjustment(Gtk.Adjustment(
            self.settings.get_complexity(), -1, 21, 1, 1, 1))
        hbox.pack_start(complexity_entry, True, True, 0)
        # add the widget to entries
        self.entries[self.settings.get_complexity_key()] = complexity_entry

        # add an event to get back values and use them
        self.connect("destroy", self.on_configure_destroy)

    def on_configure_destroy(self, widget):
        """Called when the widget is destroyed

        :type  self: GeditFlake8ConfigWidget
        :param self: Current widget

        :type  widget: GeditFlake8ConfigWidget
        :param widget: Current widget

        :rtype: None
        """

        # extract complexity form element and get complexity value
        complexity = int(
            self.entries.pop(self.settings.get_complexity_key()).get_text())

        # extract all other values
        values = {k: v.get_active() for k, v in self.entries.items()}

        # add back complexity value
        values[self.settings.get_complexity_key()] = complexity

        # update settings
        self.settings.update_setting(values)
