#-*- coding: utf-8 -*-

"""Main module of the Plugin for gedit that allow flake8 integration

:author: Sébastien CHAZALLET <s.chazallet@gmail.com>
:organization: InsPyration EURL
:copyright: Copyright © InsPyration EURL <www.inspyration.org>
:license: GPL 3 <http://www.gnu.org/licenses/gpl.html>

:version: 0.1
"""


from gettext import gettext as _

from gi.repository import GObject, Gtk, Gedit, PeasGtk

from integrated import check_code_and_get_formated_result as check_code
from settings import SettingsManager
from tree import GeditFlake8Tree
from configuration import GeditFlake8ConfigWidget

# Menu to insert in gedit's 'Tools' menu
ui_str = """<ui>
  <menubar name="MenuBar">
    <menu name="ToolsMenu" action="Tools">
      <placeholder name="ToolsOps_2">
        <menuitem name="GeditFlake8" action="GeditFlake8"/>
      </placeholder>
    </menu>
  </menubar>
</ui>
"""


class GeditFlake8WindowActivatable(GObject.Object,
                                   Gedit.WindowActivatable,
                                   PeasGtk.Configurable):
    """Main module of the gedit flake8 plugin"""

    __gtype_name__ = "GeditFlake8WindowActivatable"

    window = GObject.property(type=Gedit.Window)

    def __init__(self):
        """Initialize plugin

        :type  action: function
        :param action: Called function that we intercept prints

        :rtype:  None
        """
        GObject.Object.__init__(self)
        # Initialize settings
        self._settings = SettingsManager()
        self.flake8_tree = None

    def do_activate(self):
        """Activate the plugin

        :type  self: GeditFlake8WindowActivatable
        :param self: Current plugin

        :rtype:  None
        """
        # Get the Gtk.UIManager
        manager = self.window.get_ui_manager()

        # Create a new action group
        self._action_group = Gtk.ActionGroup("GeditFlake8PluginActions")
        self._action_group.add_actions([("GeditFlake8",
                                         None,
                                         _("Check Python source code"),
                                         None,
                                         _("Check Python source code"),
                                         self.on_check_source_code)])

        # Insert the action group
        manager.insert_action_group(self._action_group, -1)

        # Merge the UI
        self._ui_id = manager.add_ui_from_string(ui_str)

        # Create a tree
        self.flake8_tree = GeditFlake8Tree(self.window)

    def do_deactivate(self):
        """Deactivate the plugin

        :type  self: GeditFlake8WindowActivatable
        :param self: Current plugin

        :rtype:  None
        """
        # Remove the tree from the side panel
        self.flake8_tree.remove()

        # Delete tree object
        self.flake8_tree = None

        # Get the Gtk.UIManager
        manager = self.window.get_ui_manager()

        # Remove the ui
        manager.remove_ui(self._ui_id)

        # Remove the action group
        manager.remove_action_group(self._action_group)

        # Delete the action group object
        self._action_group = None

        # Make sure the manager updates
        manager.ensure_update()

    def do_update_state(self):
        """Allow to activate the plugin only if the document is python code
        If the document is a new document, it hasto be saved or the language of
        the document has to be set.

        :type  self: GeditFlake8WindowActivatable
        :param self: Current plugin

        :rtype:  None
        """
        state = False
        doc = self.window.get_active_document()
        if doc is not None:
            language = doc.get_language()
            if language is not None:
                state = language.get_id() in ["python", "python3"]
        self._action_group.set_sensitive(state)

    def do_create_configure_widget(self):
        """Action when a user want to configure the plugin

        :type  self: GeditFlake8WindowActivatable
        :param self: Current plugin

        :rtype:  GeditFlake8ConfigWidget
        :return: a configuration widget
        """
        return GeditFlake8ConfigWidget(self._settings)

    def on_check_source_code(self, action):
        """The user ask to check code source :
        1\ check there is a doc and the doc is python code
        2\ use flake8
        3\ put flake8 results into the tree and activate it

        :type  self: GeditFlake8WindowActivatable
        :param self: Current plugin

        :type  action: Gtk.Action
        :param action: Called function that we intercept prints

        :rtype:  None
        """

        # Get the document content
        doc = self.window.get_active_document()
        if not doc:
            return

        # check the language of current document is set
        language = doc.get_language()
        if language is None:
            return

        # check the language of current document is python
        if language.get_id() not in ["python", "python3"]:
            return

        # Use flake8 to get warnings
        warnings = check_code(
            doc.get_text(doc.get_start_iter(), doc.get_end_iter(), False),
            map(str.upper, self._settings.get_ignore_list()),
            self._settings.get_complexity())

        # Empty store
        self.flake8_tree.update_model(warnings)

        # Activate tree
        self.flake8_tree.activate()
