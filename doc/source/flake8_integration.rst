Auto generated documentation
============================

Plugin
------
.. automodule:: flake8_integration
   :members:
   :private-members:
   :special-members:

Integrated
----------

.. automodule:: flake8_integration.integrated
   :members:
   :private-members:
   :special-members:

Window activatable
------------------

.. automodule:: flake8_integration.windowactivatable
   :members:
   :private-members:
   :special-members:

Tree
----

.. automodule:: flake8_integration.tree
   :members:
   :private-members:
   :special-members:

Settings
--------

.. automodule:: flake8_integration.settings
   :members:
   :private-members:
   :special-members:

Configuration
-------------

.. automodule:: flake8_integration.configuration
   :members:
   :private-members:
   :special-members:

.. autoclass:: GeditFlake8ConfigWidget
   :members:
   :private-members:
   :special-members:

Status
------

This plugin is under active development.

