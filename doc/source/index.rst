.. gedit Flake8 integration documentation master file, created by
   sphinx-quickstart on Mon Apr  8 11:46:38 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gedit Flake8 integration's documentation!
====================================================

Contents:

.. toctree::
   :maxdepth: 2

   flake8_integration


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

